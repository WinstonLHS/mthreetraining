-- 1. Retrieve all the rows from FIX which have the word 'limit' in their description, your search
--    should not be case sensitive.
select * 
    from REFDATA.fix
    where upper(descr) like '%' || upper('limit') || '%';  

-- 2. 12 character ISIN codes normally start with a 2 character country code, retrieve a unique list 
--    of country codes from the instrument table
select distinct substr(isin, 1, 2) as CountryCode
    from REFDATA.instrument
    where isin is not null and 
    substr(isin, 1, 1) >= 'A'  -- check if the 1st letter value is not a letter
    and 
    substr(isin, 2, 1) >= 'A'; -- check if the 2nd letter value is not a letter
    -- Output: has XS but XS is not a Country Code, to solve this problem, you'll need to compare
    --         with a database list of Country Codes.

-- 3. Get the fills from ORDERS, show the value of each fill, show side as 'Buy' and 'Sell' instead of
--    numbers, show the largest sell first.
--    1. break this into steps
--    2. check the steps
--    3. implement the steps
select
    case side
        when '1' then 'Buy'
        when '2' then 'Sell'
        else 'Error Side'
    end as "BuySell",
    price*orderqty as totalPrice,
    posttrade.orders.*
    from posttrade.orders
    --where regexp_like(descr, 'fill', 'i') --Wrong; cause searching by a word may not show all records
    --                                        that are fill 
    where cumqty = orderqty 
    and 
    substr(parentordid, 1, 2) = 'te'    
    order by side desc, totalPrice desc; 

-- 4. Repeat(1) but use at least 4 different methods including:
--    1. REGEXP_LIKE(DESCR, 'limit', 'i'); where i is case insensitive(options other than 'i' are never
--       used normally)
select * 
    from REFDATA.fix
    where upper(descr) like '%' || upper('limit') || '%';  
select * 
    from REFDATA.fix
    where lower(descr) like '%' || lower('limit') || '%';       
select * 
    from REFDATA.fix
    where regexp_like(descr, 'limit', 'i');   -- i is case insensitive, c is case sensitive
 select *
    from Refdata.fix
    where instr(UPPER(descr), 'LIMIT') >0;
select *
    from Refdata.fix
    where instr(lower(descr), 'limit') >0;
