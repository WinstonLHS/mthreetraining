-- slide 32
SELECT
    tag
    FROM refdata.fix;
SELECT
    tag AS "fixtags"
    FROM refdata.fix;
SELECT
    *
    FROM posttrade.orders
    WHERE leavesqty = orderqty - cumqty;
SELECT
    *
    FROM posttrade.orders
    WHERE NOT leavesqty = orderqty - cumqty;
SELECT
    *
    FROM posttrade.orders
    WHERE clientid = 'bob' AND ordstatus IN (0, 2);
SELECT
    *
    FROM refdata.fix
    WHERE regexp_like(descr, 'short');
    
-- slide 37
SELECT 
    * 
    FROM refdata.instrument 
    WHERE bbid IS NOT NULL AND instrument IS NOT NULL;
SELECT 
    * 
    FROM refdata.instrument 
    WHERE substr(bbid, 1, instr(bbid, ':') - 1) = instrument;
SELECT 
    * 
    FROM refdata.instrument 
    WHERE bbid LIKE '%' || instrument || '%';
SELECT 
    * 
    FROM refdata.instrument 
    WHERE NOT regexp_like(bbid, instrument);
SELECT
    *
    FROM refdata.instrument
    WHERE NOT substr(bbid, 1, instr(bbid, ':') - 1) = instrument AND bbid LIKE '%' || instrument || '%';
    
-- slide 41
SELECT
    *
    FROM refdata.fix
    WHERE lower(descr) LIKE '%limit%';
SELECT DISTINCT
    substr(isin, 1, 2) as "Country Code"
    FROM refdata.instrument
    WHERE regexp_like(substr(isin, 1, 2), '[^0-9]');
SELECT
    id, clientid,
    CASE side
        WHEN '1' THEN 'Buy'
        WHEN '2' THEN 'Sell'
    END AS BuySell
    , price * orderqty AS value
    FROM posttrade.orders
    WHERE ordstatus IN (1, 2) AND msgtype = 'UC'
    ORDER BY BuySell DESC, value DESC;

-- slide 52
SELECT
    *
    FROM (SELECT
            id p_id, orderid p_orderid
            FROM posttrade.orders
        )
    JOIN posttrade.orders
    ON posttrade.orders.orderid = p_orderid;
SELECT
    el.empno, el.ename, el.job, er.ename, salgrade.grade, dept.dname, dept.loc
    FROM user1.emp el
    JOIN user1.salgrade
    ON el.sal BETWEEN salgrade.losal AND salgrade.hisal
    LEFT OUTER JOIN user1.emp er
    ON el.mgr = er.empno
    LEFT OUTER JOIN user1.dept
    ON el.deptno = dept.deptno
    ORDER BY grade;
SELECT
    el.empno, el.ename, el.job, er.ename, salgrade.grade, dept.dname, dept.loc
    FROM user1.emp el
    JOIN user1.salgrade
    ON el.sal BETWEEN salgrade.losal AND salgrade.hisal
    LEFT OUTER JOIN user1.emp er
    ON el.mgr = er.empno
    LEFT OUTER JOIN user1.dept
    ON el.deptno = dept.deptno
    WHERE lower(el.ename) LIKE 'a%' AND dept.dname = 'sales';

-- slide 60
SELECT
    * 
    FROM refdata.mic, refdata.instrument;
SELECT
    *
    FROM refdata.mic
    JOIN refdata.instrument
    ON mic.mic = instrument.mic;
SELECT
    *
    FROM refdata.instrument
    RIGHT JOIN refdata.mic
    ON instrument.mic = mic.mic;
SELECT avg(numOfFills) 
    FROM (SELECT
            count(*) numOfFills
            FROM posttrade.orders
            WHERE orderqty = cumqty AND parentordid LIKE 'te%'
            GROUP BY rootordid);