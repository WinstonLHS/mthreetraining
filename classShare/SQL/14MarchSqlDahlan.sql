select distinct tag from refdata.fix;

select distinct tag as fixtags from refdata.fix;

select * from posttrade.orders where leavesqty = (orderqty - cumqty);


select * from posttrade.orders where leavesqty != (orderqty - cumqty);

select * from posttrade.orders where clientid = 'bob' and ordstatus in (0,2);

select * from REFDATA.fix where regexp_like(descr,'short');
select * from refdata.fix where descr like '%short%';

select * from posttrade.orders order by side;



select instrument,bbid from refdata.instrument 
    where substr(bbid,1, instr(bbid, ':') - 1) = instrument;
    
select instrument,bbid 
    from refdata.instrument 
    where bbid like '%'||instrument||'%';
    
select value from refdata.fix;

-- 1 and 4

select * from refdata.fix where upper(descr) like upper('%limit%');
select * from refdata.fix where lower(descr) like lower('%limit%');

select * from refdata.fix where REGEXP_LIKE(descr,'limit','i');

select descr from refdata.fix where instr(lower(descr),'limit') > 0;
select descr from refdata.fix where instr(upper(descr),'LIMIT') > 0;

-- 2
select substr(isin,1,2) from refdata.instrument;
select substr(isin,1,2) as uniqueCountryCode from refdata.instrument where isin is not null;


select distinct substr(isin,1,2) as uniqueCountryCode from refdata.instrument 
    where isin is not null;


select * from
(select distinct substr(isin,1,2) as uniqueCountryCode from refdata.instrument 
    where isin is not null )
    where not regexp_like(uniqueCountryCode, '[[:digit:]]');

select * from
(select distinct substr(isin,1,2) as uniqueCountryCode from refdata.instrument 
    where isin is not null and instrument is not null )
    where not regexp_like(uniqueCountryCode, '[[:digit:]]');
    

-- 3
select * from posttrade.orders;

select side, posttrade.orders.*,
    case side
    when '1' then 'Buy'
    when '2' then 'Sell'
    else 'Error Side'
    End
    from posttrade.orders
    
    ;
    
select * from
posttrade.orders left outer join (select id p_id,orderid p_orderid from
posttrade.orders)
on parentordid=p_orderid
where rootordid='om1' order by id;

select * from user1.emp;
select * from user1.dept;
select * from user1.salgrade;

select * 
    from user1.emp
    join user1.dept
    on user1.emp.deptno = user1.dept.deptno;

select *
    from(select * 
    from user1.emp
    join user1.dept
    on user1.emp.deptno = user1.dept.deptno)
    where sal between 700 and 1200
    ;

select * from user1.emp;

select user1.emp.*, salgrade.grade
    from user1.emp
    join user1.salgrade
    on user1.emp.sal between salgrade.losal and salgrade.hisal;

 
select user1.emp.*, salgrade.grade
    from (select * 
        from user1.emp
        join user1.dept
        on user1.emp.deptno = user1.dept.deptno) 
    join user1.salgrade
    on user1.emp.sal between salgrade.losal and salgrade.hisal;   


select e1.*, e2.ename,salgrade.grade, dept.dname, dept.loc
    from user1.emp e1
    join user1.dept
    on e1.deptno = user1.dept.deptno
    join user1.salgrade
    on e1.sal between salgrade.losal and salgrade.hisal
    left join user1.emp e2
    on e1.mgr = e2.empno
    where lower(e1.ename) like 'a%' and lower(dept.dname) = 'sales'
    ;   
    





