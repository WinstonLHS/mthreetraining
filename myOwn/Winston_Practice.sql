select * from posttrade.orders;
select * from refData.instrument;
select datetime, price, price from posttrade.orders;
select side, posttrade.orders.* from posttrade.orders;
select side, * from posttrade.orders; --this syntax is wrong. use syntax above.

select datetime as OrderTime, instrument as "Symbol", side "BuySell" from posttrade.orders;
select distinct side, ordstatus from posttrade.orders;

select tag, value, tag||' '||value from refdata.fix;
select tag, value, tag||' '||value as fixtag from refdata.fix;

select price, orderqty, price*orderqty as dollarvalue from posttrade.orders;
select price, orderqty, price*orderqty as dollarvalue, cumqty/orderqty as fillratio from posttrade.orders;

select 2, 3, 2*3, 2*3+4, datetime from posttrade.orders;
select 2, 3, 2*3, 2*3+4 from dual;

select side, instrument, orderqty from posttrade.orders where side = 1;
select side, instrument, orderqty from posttrade.orders where not side = 1;
select side, instrument, orderqty from posttrade.orders where side != 1; -- same as line above.
select side, instrument, orderqty from posttrade.orders where not side <> 1; -- same as line above.
select side, instrument, orderqty from posttrade.orders where cumqty > 0;
select side, instrument, orderqty from posttrade.orders where datetime <= '18-JUL-16 08:00:00.00';

select datetime, instrument, orderqty, side, parentordid from posttrade.orders where parentordid like 'te%';
select datetime, instrument, orderqty, side, parentordid from posttrade.orders where parentordid like 'TE%';
select datetime, instrument, orderqty, side, parentordid from posttrade.orders where ordstatus between 70 and 75;
select msgtype, instrument, orderqty, side from posttrade.orders where msgtype in ('UP','UC');

select 
    rootordid, parentordid, msgtype, orderid, system, ordstatus, orderqty, cumqty, leavesqty, price, descr
    from posttrade.orders
    where rootordid='om1';

select
    clientid, datetime, instrument, orderqty, side
    from posttrade.orders
    where parentordid is null;

select
    clientid, datetime, instrument, orderqty, side, parentordid
    from posttrade.orders
    where parentordid like '_e%';
    
select
    msgtype,instrument,orderqty,side
    from posttrade.orders
    where msgtype in ('UP','UC');
    
select
    ordstatus,instrument,orderqty,side,orderid, rootordid,parentordid
    from posttrade.orders
    where side not in (1,2);

select id, msgtype, instrument, orderqty, side from posttrade.orders where id between 70 and 75;

select * from dual;
select datetime from posttrade.orders where datetime like '%03.%';
select datetime from posttrade.orders where datetime like '% 03.%';

select * from refdata.instrument where name like 'B_ %' and mic is not null;
select * from refdata.fix where regexp_like(descr,'Re[pj]');
select * from refdata.fix where regexp_like(descr,'Rep.*Rep');
select * from refdata.instrument where instrument is not null or ric is not null;


select * from refdata.fix;
select tag, value from refdata.fix;
select tag, value as fixtags from refdata.fix;
select * from posttrade.orders where leavesqty = orderqty - cumqty;
select * from posttrade.orders where leavesqty != orderqty - cumqty or (leavesqty is null or orderqty is null or cumqty is null);


select * 
    from posttrade.orders 
    where  clientid = 'bob' and (ordstatus=0  and orderid like 'te%' 
    or ordstatus=2 and parentordid like 'te%');
select * 
    from posttrade.orders 
    where (ordstatus=0  and orderid like 'te%' 
    or ordstatus=2 and parentordid like 'te%')
    order by clientid, id;

select * from posttrade.orders where  clientid = 'bob' and (ordstatus=0 or ordstatus=2) and orderid like 'te%';
select * from posttrade.orders where clientid='jeff' and (ords=0 or ordstatus=2);


select price from posttrade.orders order by price desc;
select * from refdata.fix where descr like '%short%';
select * from refdata.fix where descr like '%'||'short'||'%';
select * from refdata.fix where regexp_like(lower(descr),'sell'); -- includes both upper and lower cases.

select ric,instr(ric,'.'),substr(ric,3) from refdata.instrument;
select ric, instr(ric,'.') as targetpos, substr(ric,instr(ric,'.')), substr(ric,1+instr(ric,'.')) as mycol from refdata.instrument;
select isin, sedol, substr(isin,5,7) as shortisin from refdata.instrument where substr(isin,5,7)=sedol;

select * from refdata.instrument;
select * from refdata.instrument where isin is null or sedol is null;
/* 
    analyse the database using queries. 
    And learn the banking business in order to understand what the database values mean.
*/


select count(*) from refdata.instrument;
select * from refdata.instrument;

select * from refdata.fix where descr like '%short%';
select * from refdata.fix where descr like '%'||'short'||'%';

select instrument, bbid from refdata.instrument where bbid like '%instrument%'; --this does not return anything

select instrument, bbid from refdata.instrument where bbid like '%'||instrument||'%';

SELECT instrument, bbid 
    FROM refdata.instrument 
    WHERE instrument IS NOT NULL 
        AND bbid IS NOT NULL 
        AND bbid LIKE '%'||instrument||':%'
        OR bbid LIKE '%'||instrument||'/%:%';

select instrument, bbid from refdata.instrument 
    where substr(bbid,1,4)=instrument
    or substr(bbid,1,3)=instrument 
    or substr(bbid,1,2)=instrument;
    
select instrument, bbid from refdata.instrument;
select instrument, bbid from refdata.instrument where not (bbid like instrument||':%') or bbid is null;

SELECT USER 
from dual;
SELECT SYSDATE 
from dual;
SELECT * 
from dual;
SELECT 1+1
from dual;

select 22/7 from dual;
select round(22/7*100000)/100000 from dual;

select ceil(120398120398/10000)*10000 from dual;
SELECT SYSDATE, months_between(sysdate,'01-JAN-2000') FROM dual;
SELECT (months_between(sysdate, '01-MAY-1996'))/12 from dual; --number of years
SELECT floor(months_between(sysdate, '01-MAY-1996'))/12 from dual; --number of years
SELECT floor(floor(months_between(sysdate, '10-FEB-1992'))/12) AS yrs, 
    mod(floor(months_between(sysdate, '10-FEB-1992')),12) AS mths
    FROM dual; --how old I am
    
SELECT value,1+value from refdata.fix where value<'A';
select value from refdata.fix;

select datetime,
    to_char(datetime,'DD:MM:YYYY')
    from posttrade.orders;
    
select instrument, ric, isin, coalesce(instrument,ric,isin) as identifier from refdata.instrument;
--coalesce returns the first field which is not null

select orderqty,cumqty,ordstatus,
    case ordstatus
    when '0' then 'new'
    when '1' then 'partially filled'
    when '2' then 'fully filled'
    else 'not specified'
    end as Fillstatus
    from posttrade.orders;
select orderqty,cumqty,ordstatus,
    case ordstatus --this is a CHAR field
    when 0 then 995
    when 1 then 111
    when 2 then 333
    else 1123
    end as Fillstatus
    from posttrade.orders; --error: inconsistent datatype - expected CHAR got NUMBER

select orderqty,cumqty,ordstatus,
    case id --this is a NUMBER field
    when '0' then '999'
    when '1' then '111'
    when '2' then '222'
    else '654'
    end as Fillstatus
    from posttrade.orders; --error: inconsistent datatype - expected CHAR got NUMBER

select orderqty,cumqty,ordstatus,
    case id --this is a NUMBER field
    when 0 then 999
    when 1 then 111
    when 2 then 222
    else 654
    end as Fillstatus
    from posttrade.orders;
    
select cumqty,orderqty, 100*cumqty/orderqty||'%' from posttrade.orders;
select cumqty,orderqty, 100*cumqty/orderqty||'%' from posttrade.orders where not (cumqty is null or orderqty is null);
select cumqty,orderqty, 100*cumqty/orderqty||'%',
    case when mod(1000*cumqty/orderqty,1)>0 then '0'||100*cumqty/orderqty||'%'
    from posttrade.orders where not (cumqty is null or orderqty is null);
    
--Exercise:
/* Qn 1: Retrieve all the rows from FIX which have the word 'limit' in their description, your
    search should not be case sensitive.
    */
select * from refdata.fix where lower(descr) like '%limit%';
/* Qn 2: 12 character ISIN codes normally start with a 2 character country code, retrieve a
    unique list of country codes from the instrument table
    */
select * from refData.instrument where 1=0; --get the field names to analyse table
select * from dual where substr('V3',1,2)>='AA';-- testing
select * from dual where substr('V3',1,1)>='A' and substr('V3',2,2)>='A';-- testing
-- answer:
select distinct substr(isin,1,2) as CountryCode from refdata.instrument where isin is not null and substr(isin,1,1)>='A' and substr(isin,2,2)>='A';
-- check negation:
select distinct substr(isin,1,2) as CountryCode from refdata.instrument where not (isin is not null and substr(isin,1,1)>='A' and substr(isin,2,2)>='A');

/* Qn 3:
    Get the fills from ORDERS, show the value of each fill, show side as 'Buy' and 'Sell'
    instead of numbers, show the largest sell first.
    1. break this into steps
    2. check the steps with me
    3. implement the steps
    */
-- Qn 3.1:
select * from posttrade.orders where 1=0;
select distinct * from posttrade.orders;
select clientid,instrument,orderid,rootordid,parentordid,ordstatus,ordtype,side,orderqty,cumqty,leavesqty,price,descr
    from posttrade.orders where lower(descr) like '%fill%';
select distinct orderid,rootordid,parentordid,ordstatus,ordtype,side,orderqty,cumqty,leavesqty,price,descr
    from posttrade.orders where side=2;
select distinct ordstatus,msgtype,ordtype,side,descr from posttrade.orders where ordtype=5;
select id, datetime, clientid, system, instrument, ordtype, venue, 
    case side
        when '1' then 'Buy'
        when '2' then 'Sell'
        else 'Other'
        end as BuySell
    , orderqty,cumqty,leavesqty,price,(cumqty*price) as "Total Value", descr
    from posttrade.orders 
    where price is not null and leavesqty=0 and orderid like 'te%' and not parentordid like 'om%'
    order by side desc, "Total Value" desc; --sell side first, largest order first.
    
/* Qn 4:
    Repeat(1) but use at least 4 different methods including:
    1. REGEXP_LIKE(DESCR, 'limit','i'); --where i is case insensitive(options other than 'i' are never used
    normally)
    */
select * from refdata.fix where lower(descr) like '%limit%';
select * from refdata.fix where upper(descr) like '%'||upper(descr)||'%';
select * from refdata.fix where regexp_like(descr,'limit','i');
select * from refdata.fix where instr(lower(descr),'limit')>0;

select * from actor join actor on 1=1;
SELECT orderid,rootordid,datetime 
    FROM posttrade.orders 
    WHERE ordstatus=0 
    ORDER BY rootordid DESC, datetime 
    OFFSET 1 ROWS -- skips 1 row
    FETCH NEXT 10 ROWS ONLY; -- shows only the first 10 rows skipping the offset rows;

select refdata.instrument.instrument, posttrade.orders.instrument
    from refdata.instrument, posttrade.orders;

--old syntax for join
SELECT
    refdata.instrument.instrument,
    posttrade.orders.instrument,
    side,
    price,
    orderqty
FROM
    refdata.instrument,
    posttrade.orders
WHERE
    refdata.instrument.id = posttrade.orders.instrument;
--new syntax for join
SELECT
    refdata.instrument.instrument,
    posttrade.orders.instrument,
    side,
    price,
    orderqty
    FROM
        refdata.instrument
        JOIN posttrade.orders ON refdata.instrument.id = posttrade.orders.instrument;

--subquery
SELECT
    orderid
    FROM
        posttrade.orders
    WHERE
        ordstatus = 0 and parentordid is null
    ORDER BY
        rootordid DESC,
        datetime
    FETCH FIRST 2 ROWS ONLY;


SELECT count(*)
FROM posttrade.orders
WHERE
    rootordid IN (
        SELECT orderid
        FROM posttrade.orders
        WHERE ordstatus = 0 AND parentordid IS NULL
        ORDER BY rootordid DESC, datetime
        FETCH FIRST 2 ROWS ONLY
    );
    
SELECT id as p_id, orderid as p_orderid from posttrade.orders where ordstatus=0;
SELECT count(*) from posttrade.orders where ordstatus=0;

-- Problem explained after lunch on Day 2.
select *
    from (SELECT id AS p_id, orderid AS p_orderid FROM posttrade.orders WHERE ordstatus=0) 
    right outer join posttrade.orders
    on posttrade.orders.parentordid = p_orderid where rootordid='om1'
    order by id;
select * 
    from (SELECT id AS p_id, orderid AS p_orderid FROM posttrade.orders WHERE ordstatus=0),posttrade.orders
    where parentordid=p_orderid(+) and rootordid='om1' 
    order by id;
    
select count(*)
    from refdata.instrument, posttrade.orders
    where orders.instrument=instrument.id;
    
select * from user1.emp where 1=0;

--Exercise:
/* Qn 3:
    determine the name of everyone's manager, and then try joining the
    other tables:
    user1.emp
    user1.dept
    user1.salgrade
    */
select distinct managers.empno,managers.ename, managers.sal
    from user1.emp managers
    join user1.emp employees
    on employees.mgr = managers.empno
    order by managers.sal desc;

/* Qn 4:
    Which employees, whose names start with 'a', work
    in the sales department?
    */
select distinct
    employees.empno, employees.ename AS "Employee Name", departments.dname AS "Department Name"
    from user1.emp employees
    join user1.dept departments
    on employees.deptno=departments.deptno and departments.dname = 'sales'
    where lower(substr(employees.ename,1,1))='a';

select distinct managers.empno,managers.ename, managers.sal
    from user1.emp managers
    join user1.emp employees
    on employees.mgr = managers.empno
    order by managers.sal desc
    ;
    
select sum(orderqty*price) AS DollarValue,
    count(*) numCompleteOrders
    from posttrade.orders
    where price is not null and leavesqty=0 and orderid like 'te%' and not parentordid like 'om%';
    --where orderqty=cumqty and parentordid like 'te%';
    
select distinct t1.orderid, t1.parentordid,t1.descr from posttrade.orders t1 join posttrade.orders t2 on t1.orderid!=t2.orderid or t1.parentordid!=t2.parentordid;

-- completed orders for a particular stock
select
    case side
        when '1' then 'Buy'
        when '2' then 'Sell'
        end as BuySell,
    refdata.instrument.name,
    count(*) NumCompletedOrders,
    sum(cumqty*price) DollarValue
    from posttrade.orders join refdata.instrument on posttrade.orders.instrument=refdata.instrument.id
    --where leavesqty=0 and posttrade.orders.parentordid like 'te%'
    where price is not null and leavesqty=0 and orderid like 'te%' and not parentordid like 'om%'
    group by side,instrument.name
    order by DollarValue desc;
-- Exercise slide 60/94
/* Qn 1:
    Do a Cartesian join between MIC and INSTRUMENT
    */
select count(*) from refdata.instrument, refdata.mic;
select count(*) from refdata.instrument full outer join refdata.mic on 1=1;
/* Qn 2:
    Do an equi join between MIC and INSTRUMENT
    */
select * from refdata.instrument join refdata.mic on refdata.instrument.mic=refdata.mic.mic;
/* Qn 3:
    Do a right join between MIC and INSTRUMENT, with MIC being on the
    right(enrich MIC with INSTRUMENT)
    */
select * from refdata.instrument right join refdata.mic on refdata.instrument.mic=refdata.mic.mic;
/* Qn 4:
    What is the average number of fills required to complete an order?
    */
select * from posttrade.orders;
--this query returns the list of separate orders.
select
    case side
        when '1' then 'Buy'
        when '2' then 'Sell'
        end as BuySell,
    refdata.instrument.name,
    count(*) NumCompletedOrders,
    sum(cumqty*price) DollarValue
    from posttrade.orders join refdata.instrument on posttrade.orders.instrument=refdata.instrument.id
    where leavesqty=0 and posttrade.orders.parentordid like 'te%'
    group by side,instrument.name, posttrade.orders.parentordid
    order by DollarValue desc;