/*
Qn 18:
List all employees that work in the Sales department, 
along with the name of their manager, their base salary. 
Create an additional column showing their bonus earnings 
should they each sell �500, 000 worth of goods in a year. 
Show their Overall Total Earning (OTE) in the last column, 
sorted in descending order.
*/

-- firstly, we get a table of all the employees in the sales dept, their manager id, and their base salary and commission
select ename, dname, departments.deptno, mgr, sal, comm
from user1.emp employees
join user1.dept departments
on employees.deptno = departments.deptno
where departments.dname = 'sales';
-- we get about 6 people in the sales dept
-- next, we get their manager names along on the same table.
select salesEmployees.ename as Employee, salesEmployees.dname as Department, salesEmployees.sal as Salary, 
salesEmployees.comm as Commission, managers.ename as Manager,
salesEmployees.sal + coalesce(salesEmployees.comm,0) as "Overall Total Earning"
from (select ename, dname, departments.deptno, mgr, sal, comm
    from user1.emp employees 
    join user1.dept departments 
    on employees.deptno = departments.deptno 
    where departments.dname = 'sales') salesEmployees
join user1.emp managers
on salesEmployees.mgr = managers.empno
order by "Overall Total Earning" desc;
-- this table orders the list in descending order of how much income they have in total.
-- it is not clear what the commission rate is for sales, so we simply not everybody's Overall Total Earning regardless.

/*
Qn 19:
Which manager has the most number of employees reporting to them? Provide the manager name, 
their job title, department and the number of employees reporting to them.
*/
-- select all the managers empno first with their managed employees next to them.
select distinct managers.ename, managers.empno, managers.job, managers.deptno,
employees.ename, employees.empno
from user1.emp managers
join user1.emp employees
on employees.mgr = managers.empno
order by managers.empno;
-- there are 6 managers

-- next, group managers according to employees from above query
select managerNames, MgrJob, departments.dname as MgrDept, count(*) as NumEmployees
from (select distinct managers.ename as managerNames, managers.empno as managerIds,  managers.job as MgrJob, 
    managers.deptno as MgrDeptNo,
    employees.ename as employeeNames, employees.empno as employeeID
    from user1.emp managers
    join user1.emp employees
    on employees.mgr = managers.empno
    order by managers.empno) managers2
join user1.dept departments
on managers2.MgrDeptNo = departments.deptno
group by managerIds, managerNames, MgrJob, departments.dname
order by NumEmployees desc;
-- the only thing left is to pick the very top one.

-- this is another attempt using subquery
select managerNames, MgrJob, MgrDept, count(*) as NumEmployees
from (select managerIds, managerNames, MgrJob, departments.dname as MgrDept, employeeNames, employeeID
    from (select distinct managers.ename as managerNames, managers.empno as managerIds,  managers.job as MgrJob, 
        managers.deptno as MgrDeptNo,
        employees.ename as employeeNames, employees.empno as employeeID
        from user1.emp managers
        join user1.emp employees
        on employees.mgr = managers.empno
        order by managers.empno) managers2
    join user1.dept departments
    on managers2.MgrDeptNo = departments.deptno)
group by managerIds, managerNames, MgrJob, MgrDept
order by NumEmployees desc;


-- all we do is add a filtering condition to select only the top guy with the most employees under him/her.
select managerNames, MgrJob, departments.dname as MgrDept, count(*) as NumEmployees
from (select distinct managers.ename as managerNames, managers.empno as managerIds,  managers.job as MgrJob, 
    managers.deptno as MgrDeptNo,
    employees.ename as employeeNames, employees.empno as employeeID
    from user1.emp managers
    join user1.emp employees
    on employees.mgr = managers.empno
    order by managers.empno) managers2
join user1.dept departments
on managers2.MgrDeptNo = departments.deptno
group by managerIds, managerNames, MgrJob, departments.dname
order by NumEmployees desc
fetch first row only;



/*
Qn 20:
What is the average salary paid for employees in each department? 
Include region, department  name and average salary 
*/

select dname, avg(sal), loc
from user1.emp employees
join user1.dept departments
on employees.deptno = departments.deptno
group by departments.deptno, departments.dname, departments.loc;